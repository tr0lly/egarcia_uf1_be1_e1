# Exercicis UF1 . Programació estructurada
## 1.1 Projectes de desenvolupament d'aplicacions. Entorn integre de desenvolupament. Exercici 1:
### Cerca al mercat actual un IDE propietari i un codi lliure on pugui programar en ANSI C

Un IDE és més que un simple editor de text sino que ens permeten no només escriure codi font , sinó també generar-hi un programa executable , per exemple , per a programar en ANSI C amb un IDE , podem destacar programes com ara el Microsoft Visual Studio , mentre que de codi lliure tenim el CodeBlocks .

### Creus que el CodeBlocks és un bon IDE per a programar en C?

Si, ja que al ser de Codi Liure, és més senzill i còmode. A part , també té integrat un sistema de previsualització del codi que has realitzat i també, gràcies a una consola on et marque: el error, la línea i el problema concret, és més fàcil detectar si has fet algun error i poder arreglar-lo.

## Blocs d'un programa informàtic. Exerici 2:
### Mostra els blocs de codi d'un programa en ANSI C. Comenta'ls breument. Quins blocs apareixeran com a mínim en tot programa C?

Un bloc d'instruccions és UNA instrucció composta de diverses instruccions encadenades, per exemple, un bloc que mai pot faltar és el **#include <stdio.h>**, el qual es posa al inici del programa i que conté les definicions de les macros , les constants...

Un alte codi es el **int main()**, la funció main és imprescindible en qualsevol programa C/C++ representa el punt d'inici de la seva execució.

Finalment tenim el codi que utilitzem al final, **Return 0;** el qual s'utilitza per indicar que el programa s'ha executat amb èxit. Qualsevol altre valor suggereix que ha passat un error.

## Comentaris al codi. Exercici 3:
### Explica els tipus de comentaris existents en C i fes un algorisme que exemplifiqui el seu ús.

A el llenguatge de programació C hi han dos tipus de comentaris, els comentaris de una sola frase, els quals se representen utilitzant // abans de escriure el comentari.
Un altre tipus de comentari que podem utilitzar, es el comentari de mes de una frase, el qual es representa utilitzant /* al inici i */ al final.

![COMENTARI](COMENTARI.PNG)

## Variables. Tipus i utilitat. Exercici 4:
### Quins són els tipus de dades simples en C?

**Caràcters**: char.

**Enters**: short , int i long.

**Nombres amb decimals**: float i double.

### Feu un programa que escrigui a pantalla un valor per cada un dels tipus simples. En els tipus numèrics no heu d’incloure els modificadors short, long, signed i unsigned

![SIMPLES](TIPUS_SIMPLES.PNG)

### Explica i posa un exemple de desbordament d’una variable de tipus enter? Afegiu una captura de la sortida a pantalla.

Un desbordament de memòria es duu a terme quan un programa informàtic excedeix l'ús de quantitat de memòria assignat pel sistema operatiu, es a dir, utilitza mes memoria de la que nosatres li hem donat. Per exemple: el numero 2147483647 es el maxim i al sumarli +1 es desborda i aquest es torna negatiu, ja que no hi ha més espai per agafar.

![DESBORD](DESBORD.PNG)

## Formats de sortida. Instrucció printf. Exercici 5: 
### Feu un programa amb la següent sortida a pantalla:

El número 77 en octal

![OCTAL](OCTAL.PNG)

El número 65535 en hexadecimal i en majúscules.

![HEXA](HEXA.PNG)

El número 32727 en hexadecimal i en minúscules.

![hexa](hexaa.PNG)

El número 7325 amb el signe.

![SIMB](SIGN.PNG)

El número 6754 amb deu dígits i farciment de 0s per l’esquerra.

![FARC](FARCIMENT.PNG)

El número 456.54378 amb 3 decimals.

![DECIM](DECIMAL.PNG)

El teu nom i cognoms.

![NOOMCOGNOMS](nomCognoms.png)

Els primers 8 caràcters de la sortida del punt anterior.

![VuitCaract](nomVuitCaracters.png)


## Constants. Tipus i utilització. Exercici 6:
### Explica que són les constants i per a que serveixen.

Una **constant** es un número o caràcter el qual podem utilitzar com a valor en un programa, amb la peculiaritat de que es un valor el qual no varia en cap moment. Serveixen per a otorgar un valor concret i ja no tenir que preocuparse més per ell, ja que tindra el mateix valor en tot el programa.

### Feu un programa que exemplifiqui l’ús de constants.

![CONSTANTS](CONSTANTE.PNG)

## Utilització de variables. Exercici 7:
### Explica que són les variables i per a que serveixen. Relaciona la memòria de la computadora per explicar-ho. Posa’n exemples.

Una **variable** es un número o caràcter on es pot emmagatzemar temporalment una dada, però en aquest cas, el valor que li donem pot variar, és a dir, li podem donar un valor determinat i mes endavant canviar-ho.

### Quan utilitzem les variables?

Les variables les utilitzem simplement per estalviar memòria en alguns casos, com per exemple en alguns casos en què la variable que utilitzarem sabem amb certesa que no prendrà valors molt grans.

### Quina relació hi ha entre una variable i la seva adreça de memòria?

La memòria es com una sèrie de caixes, cada caixa del clòset té un número que ens ajuda a distingir-les una de l'altra, aquestes seccions representarien l'espai en memòria i el número que té cada capseta, és la direcció de memòria , és a dir l'identificador que ajuda a saber on són les teves coses.

## Operadors del llenguatge de programació. Exercici 8:
### Cita en forma de taula els operadors relacionals, els aritmètics i els lògics que s’utilitzen en ANSI C.

![OPERADORS](OPERADORS.PNG)

### Què és la preferència dels operadors? Posa’n exemples

![PREFERENCIA](PREFF.PNG)


